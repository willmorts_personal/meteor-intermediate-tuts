Router.route('/', function () {
    GAnalytics.pageview();
    this.render('HomeLayout');
});

Router.route('/recipe-book', {
    name: 'recipe-book', 
    action() {
        GAnalytics.pageview();
        this.render('MainLayout', {data: {main: 'RecipesTemplate'}})
    }
});