import SimpleSchema from 'simpl-schema'
SimpleSchema.extendOptions(['autoform']);

export const Recipes = new Mongo.Collection('recipes');
Recipes.allow({
    insert: function(userId, doc) {
        return !!userId;
    }
});

Ingredient = new SimpleSchema({
    name: {
        type: String
    },
    amount: {
        type: String
    }
});

Recipes.attachSchema(new SimpleSchema({
    name: {
        type: String, 
        label: "Name"
    },
    description: {
        type: String, 
        label: "Description"
    },
    ingredients: {
        type: Array
    },
    'ingredients.$': {
        type: Ingredient
    },
    inMenu: {
        type: Boolean, 
        defaultValue: false, 
        optional: true,
        autoform: {
            type: "hidden"
        }
    },
    author: {
        type: String, 
        label: "Author",
        autoValue: function() {
            return this.userId
        },
        autoform: {
            type: "hidden"
        }
    },
    createdAt: {
        type: Date,
        label: "Created at",
        autoValue: function() {
            return new Date()
        },
        autoform: {
            type: "hidden"
        }
    }
}, { tracker: Tracker }));