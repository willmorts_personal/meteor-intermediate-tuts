import { Recipes } from "../../collections/recipes.js";

Template.NewRecipe.helpers({
    Recipe: function() {
        return Recipes;
    }
})