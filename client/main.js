import SimpleSchema from 'simpl-schema'
SimpleSchema.extendOptions(['autoform']);
import 'meteor/aldeed:autoform/static'

import { AutoFormPlainTheme } from 'meteor/communitypackages:autoform-plain/static'
AutoFormPlainTheme.load()
AutoForm.setDefaultTemplate('plain')

import './layouts/HomeLayout.html';
import './layouts/MainLayout.html';
import '../lib/router.js';
import './recipes/NewRecipe.html';
import './recipes/NewRecipe.js';
import './recipes/Recipes.html';
import './recipes/Recipes.js';

Meteor.subscribe('recipes');