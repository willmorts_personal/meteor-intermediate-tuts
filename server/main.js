import { Meteor } from 'meteor/meteor';
// Need the below so /recipes/insert can be found on the client side
import { Recipes } from '../collections/recipes.js';

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.publish('recipes', function() {
  return Recipes.find({author: this.userId});
});